// import the required modules
import {dom} from '../dom/dom';
import {ElementData} from '../dom/ElementData';
import {events} from '../events/events';
import EventManager from '../events/EventManager';

/**
 * [toggle description]
 * @type {[type]}
 */
let toggle =
{
	/**
	 * initialize the toggle
	 * @param  {HTMLElement} root	Parent element to initialize all the toggle in
	 * @return null
	 */
	init : function(root = document)
	{
		// parameters used within this init function, these relate to html/css/js names
		let params =
		{
			data_name			: 'toggle'
		};

		// TODO: make sure that this doesn't get added every time the function is initialized
		// add a watch for the hash changing, this allows us to open toggles from any link on the site
		// it also allows the back qutton to work to hide/show toggles
		EventManager.add_action('hash_change', function(hash)
		{
			EventManager.do_action(params.data_name + hash);
		});

		// get all of the items in the page
		let toggles				= root.querySelectorAll('[data-' + params.data_name + ']');

		// an object to hold all of the groups - so we can work out if the toggle should be open or not
		let groups				= {};

		// loop through them
		[].forEach.call(toggles, function (toggle)
		{
			// default settings for the toggle
			let defaults =
			{
				group			: false,
				state			: 'open',
				// private
				_init			: false
			};

			// the the json embedded data
			let options			= ElementData.get(params.data_name, toggle, defaults);

			// if the toggle has already been initialized... bail out and don't reinit
			if(options._init)
			{
				return;
			}

			// if the toggle has a group, there have been no toggles in the group already opened, and the toggle should be opened
			if(options.group)
			{
				if(options.state === 'open' && !groups[options.group])
				{
					groups[options.group]	= true;
				}
				else
				{
					options.state			= 'close';
				}
			}

			let open = function()
			{
				// if there is a group associated with the toggle,
				if(options.group)
				{
					EventManager.do_action(params.data_name + ":close#" + options.group);

					dom.addClass(toggle, 'open');
					dom.removeClass(toggle, 'close');
				}
				else
				{
					console.log("KJGH");

					if(dom.containsClass(toggle, 'open'))
					{
						dom.removeClass(toggle, 'open');
						dom.addClass(toggle, 'close');
					}
					else
					{
						dom.addClass(toggle, 'open');
						dom.removeClass(toggle, 'close');
					}
				}
			};

			let close = function()
			{
				dom.removeClass(toggle, 'open');
				dom.addClass(toggle, 'close');
			};

			// update the operlay to note that it has been initialized, ideally
			// this can be avoided by calling the init method with a specific root element
			ElementData.set(params.data_name, toggle, {_init: true});

			// if there is an id on the toggle, create a method to allow fot he togglr to be triggered
			if(toggle.id)
			{
				EventManager.add_action(params.data_name + "#" + toggle.id, open);
			}

			// if there is a group add the ability to close all accordions in the group
			if(options.group)
			{
				EventManager.add_action(params.data_name + ":close#" + options.group, close);
				EventManager.add_action(params.data_name + ":open#" + options.group, open);
			}

			dom.addClass(toggle, options.state);

			events.on(toggle, 'click', open);
		});
	}
};

export default toggle;
